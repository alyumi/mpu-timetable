import requests
import timetable_data
from requests import cookies
group = []

def is_active() -> bool:
    r = requests.get('https://rasp.dmami.ru/site/')
    if r.status_code == requests.codes.ok:
        return True
    else:
        print(r.status_code)
        return False

class TimeTable:
    def __init__(self, group: str, session: str):
        print('Site is Active: ', is_active())
        self.url = 'https://rasp.dmami.ru/site/group'
        self.group = group
        self.session = session
        self.table = self._get_json()

    def _is_active(self) -> bool:
        if self.table['status'] == 'ok':
            return True
        elif self.table['status'] == 'error':
            return False

    def _get_json(self) -> dict:
        self.headers = {'accept' : '*/ *',
                    'accept-encoding' : 'gzip, deflate, br',
                    'accept-language' : 'ru-RU, ru; q = 0.9, en - US; q = 0.8, en; q = 0.7',
                    'referer' : 'https://rasp.dmami.ru/',
                    'sec-fetch-dest' : 'empty',
                    'sec-fetch-mode' : 'cors',
                    'sec-fetch-site' : 'same-origin',
                    'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36 OPR/67.0.3575.115',
                    'x-requested-with' : 'XMLHttpRequest'
        }

        data = {'group': self.group, 'session': self.session}
        r = requests.get(self.url, headers=self.headers, params=data)
        return r.json()

    def _get_group_timetable_info(self) -> dict:
        if self._is_active():
            try:
                return self.table['group']
            except:
                print("No timetable for this group")
        else:
            print('No timetable')

    def _get_gnum(self) -> dict:
        gnum = self._get_group_timetable_info()
        try:
            return gnum['title']
        except:
            print("This group isn't existing")

    def print_gnum(self):
        global group
        p = self._get_gnum()
        if p:
            group.append(self._get_gnum())
        print(self._get_gnum())


def show_timetable(group: str, session: str):
    tt = TimeTable(group, session)
    tt.print_gnum()

def parse_existing_groups():
    for i in range(len(timetable_data.groups)):
        for elem in timetable_data.groups[i]:
            show_timetable(elem, '0')


def main():
    print('Введите номер группы (Пример: 191-351, формат 1**-***): ', sep=" ")
    number = str(inout())
    show_timetable('191-351', '0')

if __name__ == '__main__':
    main()

